using System.ComponentModel.DataAnnotations;

namespace DnDPlayerReference.Models
{
    public class Player 
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Race { get; set; }

        [Required]
        [StringLength(50)]
        public string Class { get; set; }

        [Range(1, 20)]
        public int Level { get; set; }

        [Range(1, 20)]
        public int Strength { get; set; }

        [Range(1, 20)]
        public int Dexterity { get; set; }

        [Range(1, 20)]
        public int Constitution { get; set; }

        [Range(1, 20)]
        public int Intelligence { get; set; }

        [Range(1, 20)]
        public int Wisdom { get; set; }

        [Range(1, 20)]
        public int Charisma { get; set; }
    }
}